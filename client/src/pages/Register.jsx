import styled from "styled-components";
import { mobile } from "../responsive";
import { useState, useEffect, useContext } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background:
    url("https://c4.wallpaperflare.com/wallpaper/357/77/305/neon-genesis-evangelion-ayanami-rei-asuka-langley-soryu-ikari-shinji-wallpaper-preview.jpg")
      center;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 37%;
  padding: 20px;
  background-color: #a5bdd6;
  ${mobile({ width: "75%" })}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 20px 10px 0px 0px;
  padding: 10px;
`;

const Agreement = styled.span`
  font-size: 12px;
  margin: 20px 0px;
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: #6e3e58;
  color: white;
  cursor: pointer;
`;



export default function Register (){

  const {user} = useContext(UserContext);
  const history = useHistory();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  const [isActive, setIsActive] = useState(false);

  function registerUser(e){
    e.preventDefault();
    fetch('https://radiant-lake-02226.herokuapp.com/api/users/checkEmail', {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
       body: JSON.stringify({
            email: email
        })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

        if(data === true){

          Swal.fire({
            title: 'Duplicate email found',
            icon: 'error',
            text: 'Kindly provide another email to complete the registration.'  
          });

        }else{
          fetch('http://localhost:5000/api/auth/register', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                username: username,
                password: password1
            })
              })
          .then(res => res.json())
          .then(data => {
            console.log(data);

            if (data === true) {

                // Clear input fields
                setFirstName('');
                setLastName('');
                setUsername('')
                setEmail('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                    title: 'Registration successful',
                    icon: 'success',
                    text: 'Welcome to Eva!'
                });

                history.push("/login");
                  } else {

                Swal.fire({
                    title: 'Something wrong',
                    icon: 'error',
                    text: 'Please try again.'   
                });

            }
          })
        }
    })
  }

useEffect(() => {
    // Validation to enable the submit buttion when all fields are populated and both passwords match
    if((firstName !== '' && lastName !== ''&& username !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [firstName, lastName, username, email, password1, password2])

  return (
    (user.id !== null) ?
      <Redirect to="/" />
    :
    <Container>
      <h1>Register</h1>
      <Form className="mt-3" onSubmit={(e) => registerUser(e)}>
        <Form.Group controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control 
                type="text" 
                placeholder="Enter first name"
                value={firstName} 
                onChange={e => setFirstName(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control 
                type="text" 
                placeholder="Enter last name"
                value={lastName} 
                onChange={e => setLastName(e.target.value)}
                required
            />
        </Form.Group>


        <Form.Group controlId="gender">
            <Form.Label>username</Form.Label>
            <Form.Control 
                type="text" 
                placeholder="Enter username"
                value={username} 
                onChange={e => setUsername(e.target.value)}
                required
            />
        </Form.Group>



        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control 
            type="email" 
            placeholder="Enter email" 
            value = {email}
            onChange = { e => setEmail(e.target.value)}
            required 
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>



        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Password" 
            value={password1}
            onChange = { e => setPassword1(e.target.value)}
            required 
          />
        </Form.Group>
        
        <Form.Group className="mb-3" controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password2}
            onChange = { e => setPassword2(e.target.value)}
            required 
          />
        </Form.Group>
      {/* Conditionally render submit button based on isActive state */}
        { isActive ? 
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          :
            <Button variant="primary" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
        }
        
      </Form>
    </Container>
  );
};


/*
   <Container>
      <Wrapper>
        <Title>CREATE AN ACCOUNT</Title>
        <Form>
          <Input placeholder="first name" />
          <Input placeholder="last name" />
          <Input placeholder="username" />
          <Input placeholder="email" />
          <Input type="password" placeholder="password" />
          <Input type="password" placeholder="confirm password" />
          <Agreement>
            By creating an account, I consent to the processing of my personal
            data in accordance with the <b>PRIVACY POLICY</b>
          </Agreement>
          <Button>CREATE</Button>
        </Form>
      </Wrapper>
    </Container>

*/